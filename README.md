# README

Static Vue.js app. Just build and open web/index.html.

# Building

Build assets.

```
npm install
npm run build
```

# Dev

Watching changes in assets and build them.

```
npm install
npm run dev
```
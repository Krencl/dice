var gulp = require('gulp');
var concat = require('gulp-concat');
var less = require('gulp-less');
var replace = require('gulp-replace');
var sourcemaps = require('gulp-sourcemaps');

var version = Date.now();

gulp.task('less', function () {
	return gulp.src('assets/less/main.less')
		.pipe(sourcemaps.init())
		.pipe(less())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('web/css'));
});

gulp.task('js', function() {
	return gulp.src([
		'assets/js/class/*.js',
		'assets/js/app/*.js'
	])
		.pipe(sourcemaps.init())
		.pipe(concat('main.js'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('web/js'));
});

gulp.task('html', function() {
	return gulp.src([
		'assets/html/*.html'
	])
		.pipe(replace('#VERSION#', version))
		.pipe(gulp.dest('web'));
});

gulp.task('watch', ['default'], function() {
	gulp.watch('assets/less/**/*.less', ['less', 'html']);
	gulp.watch('assets/js/**/*.js', ['js', 'html']);
	gulp.watch('assets/html/**/*.html', ['html']);
});

gulp.task('default', ['less', 'js', 'html'], function() {

});

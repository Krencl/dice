Vue.config.keyCodes.reset = 34;

var dice = new Vue({
	el: '#dice',
	data: {
		availablePlayers: ['MM', 'TK', 'HB', 'GP', 'MP', 'VD', 'JO', 'MS', 'MZ'],
		settings: {},
		game: {}
	},
	created: function() {
		// load settings
		var settings = window.localStorage.getItem('settings');
		if (settings === null) {
			this.resetSettings();
		} else {
			this.settings = JSON.parse(settings);
		}
		// load game
		var game = window.localStorage.getItem('game');
		if (game === null) {
			this.resetGame();
		} else {
			this.game = JSON.parse(game);
			this.modifyCurrentScore(0);
		}
		if (this.game.running) {
			this.focusScore();
		}
	},
	methods: {
		isSelected: function(player) {
			return this.settings.selectedPlayers.indexOf(player) !== -1;
		},
		toggleSelected: function(player) {
			var index = this.settings.selectedPlayers.indexOf(player);
			if (index === -1) {
				this.settings.selectedPlayers.push(player);
			} else {
				this.settings.selectedPlayers.splice(index, 1);
			}
		},
		startGame: function() {
			this.resetGame();
			// create players
			this.game.players = [].concat(this.settings.selectedPlayers).map(function(player){
				return {
					'name': player,
					'score': 0,
					'rounds': []
				};
			});
			this.game.players = _.shuffle(this.game.players);
			this.game.finalScore = this.settings.finalScore;
			this.game.running = true;
			this.prepareRound();
			this.focusScore();
		},
		resetGame: function() {
			this.game = {
				running: false,
				shootout: false,
				end: false,
				currentPlayer: 0,
				currentScore: null,
				currentRound: 0,
				finalScore: 0,
				winningScore: 0,
				players: [],
			};
		},
		resetSettings: function() {
			this.settings = {
				selectedPlayers: [],
				finalScore: 2000
			};
		},
		saveRound: function() {
			this.modifyCurrentScore(0);

			this.currentPlayer.rounds.push(this.game.currentScore);
			this.currentPlayer.score += parseInt(this.game.currentScore);

			if (this.currentPlayer.score >= this.winningScore) {
				this.winningScore = this.currentPlayer.score;
			}

			this.game.currentScore = null;
			if (this.game.currentPlayer === this.game.players.length - 1) {
				this.prepareRound();
			} else {
				this.game.currentPlayer++;
			}

			this.focusScore();
		},
		undoRound: function() {
			if (this.game.players[0].rounds.length === 0) {
				this.focusScore();
				return;
			}
			this.game.currentScore = null;
			this.game.currentPlayer--;
			if (this.game.currentPlayer < 0) {
				this.game.currentPlayer = this.game.players.length - 1;
				this.game.currentRound--;
			}
			this.currentPlayer.score -= this.currentPlayer.rounds.pop();
			this.focusScore();
		},
		prepareRound: function() {
			if (this.winners.length === 1) {
				this.game.end = true;
			} else {
				this.game.currentRound++;
				if (this.winners.length > 1) {
					this.game.shootout = true;
				}
				this.game.currentPlayer = 0;
			}
		},
		getScoreToWin: function(score) {
			return this.winningScore - score;
		},
		focusScore: function() {
			setTimeout(function(){
				$('#currentScore').focus();
			}, 200);
		},
		modifyCurrentScore: function(score) {
			if (this.game.currentScore === null || this.game.currentScore === '') {
				this.game.currentScore = 0;
			} else {
				this.game.currentScore = parseInt(this.game.currentScore);
			}
			this.game.currentScore += score;
		},
		setCurrentScore: function(score) {
			this.game.currentScore = 0;
		},
	},
	computed: {
		currentPlayer: function() {
			return this.game.players[this.game.currentPlayer];
		},
		winningScore: function() {
			return this.game.players.reduce(function(max, player){
				return (player.score > max ? player.score : max);
			}, this.game.finalScore);
		},
		maxScore: function() {
			return this.game.players.reduce(function(max, player){
				return (player.score > max ? player.score : max);
			}, 0);
		},
		winners: function() {
			var me = this;
			return this.game.players.filter(function(player){
				return player.score >= me.winningScore;
			});
		}
	},
	watch: {
		game: {
			handler: _.debounce(function () {
				window.localStorage.setItem('game', JSON.stringify(this.game));
			}, 200),
			deep: true
		},
		settings: {
			handler: _.debounce(function () {
				window.localStorage.setItem('settings', JSON.stringify(this.settings));
			}, 200),
			deep: true
		}
	}
});
